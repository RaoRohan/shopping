package com.tomtom.shopping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.tomtom.shopping.dao.UserAddressRepository;
import com.tomtom.shopping.dao.UserRepository;
import com.tomtom.shopping.entity.Cart;
import com.tomtom.shopping.entity.User;
import com.tomtom.shopping.entity.UserAddress;
import com.tomtom.shopping.entity.UserPayment;

@SpringBootApplication
public class ShoppingApplication implements ApplicationRunner{

	@Autowired
	UserRepository userRepo;
	//UserAddressRepository
	@Autowired
	UserAddressRepository userAddRepo;
	
	public static void main(String[] args) {
		SpringApplication.run(ShoppingApplication.class, args);
	}
	
    @Override
    public void run(ApplicationArguments arg0) throws Exception {
       System.out.println("Hello World from Application Runner");
       
       
       UserAddress address = new UserAddress();
       address.setAddressId(201);
       address.setCity("Pune");
       address.setCountry("India");
       address.setPostalcode("411057");
       UserAddress addresstwo = new UserAddress();
       addresstwo.setAddressId(202);
       addresstwo.setCity("Mumbai");
       addresstwo.setCountry("India");
       addresstwo.setPostalcode("10023");
       UserPayment paymentDetailFirstUser = new UserPayment();
       paymentDetailFirstUser.setAccountNumber("X0123567");
       paymentDetailFirstUser.setBalance(4000);
       UserPayment paymentDetailSecondUser = new UserPayment();
       paymentDetailSecondUser.setAccountNumber("X0123567");
       paymentDetailSecondUser.setBalance(4000);
       User user = new User(101,"rohanrao","rohanrao.01@gmail.com","xyz0101",
    		   "9011661694",address, paymentDetailFirstUser);
       
       User usertwo = new User(102,"rahulrao","rahulrao.01@gmail.com","xabc0101",
    		   "8298280300",addresstwo, paymentDetailSecondUser);
       
      Cart cart = new Cart();
      user.setCart(cart);
      Cart carttwo = new Cart();
      usertwo.setCart(carttwo);
      userRepo.save(user);
      userRepo.save(usertwo);
    }

}
