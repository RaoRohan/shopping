package com.tomtom.shopping.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "USER_ADDRESS")
public class UserAddress {
	
	@Id
	private int addressId;
	
	private String city;
	
	private String postalcode;
	
	private String country;
	
	public UserAddress(){
		
	}

	
	public UserAddress(int addressId, String city, String postalcode,
			String country) {
		super();
		this.addressId = addressId;
		this.city = city;
		this.postalcode = postalcode;
		this.country = country;
	}


	
	public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalcode() {
		return postalcode;
	}

	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	
	

}
