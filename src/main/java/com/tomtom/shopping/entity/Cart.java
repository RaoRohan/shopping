package com.tomtom.shopping.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name = "CART")
public class Cart {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long cartId;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="cart_id", referencedColumnName="cartId")
    private List<Product> products;

	private double subTotal;

	/*@OneToOne(mappedBy="",cascade = CascadeType.ALL)
	private User user;
*/
	
	@Transient
	public void addProductToCart(Product p){
		if(this.products.stream().anyMatch( e-> e.getProductId() == p.getProductId()))
			updateProductQuantity(p);
		else
			this.products.add(p);
	}
	
	private void updateProductQuantity(Product p){
		for(Product temp: this.products){
			if(temp.getProductId() == p.getProductId()){
				temp.setQuantity(temp.getQuantity()+p.getQuantity());
			}
		}
		
	}
	
	@Transient
	public double getUpdatedSubTotal() {
		double sum = 0;
		for(Product p: this.products){
			sum = sum + p.getPrice() * p.getQuantity();
		}
		return sum;
	}
	public Long getCartId() {
		return cartId;
	}

	public void setCartId(Long cartId) {
		this.cartId = cartId;
	}

	
	public double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	

}
