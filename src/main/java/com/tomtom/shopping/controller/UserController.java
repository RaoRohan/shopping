package com.tomtom.shopping.controller;

import java.util.List;
import java.util.Optional;

import lombok.extern.slf4j.Slf4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tomtom.shopping.dao.UserRepository;
import com.tomtom.shopping.entity.Cart;
import com.tomtom.shopping.entity.User;
import com.tomtom.shopping.model.CartProduct;
import com.tomtom.shopping.service.ProductService;
import com.tomtom.shopping.service.UserService;

@Slf4j
@RestController
public class UserController {
	
	Logger log = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private UserService userService;
	
	@PostMapping("/add/User")
	public User postUser(@RequestBody User user){
		 return userRepo.save(user);
	}
	
	@GetMapping("/users")
	public List<User> getAllUsers(){
		return userRepo.findAll();
	}
	
	@PostMapping("/userCart/addProducts/")
	public boolean postUser(@RequestBody CartProduct cProduct){
		Optional<User> optionalUser = userRepo.findById(cProduct.getUserId());
		if(!optionalUser.isPresent()){
			log.error("Invalid userId");
			return false;
		}
		else{
			User user = optionalUser.get();
			return productService.addProductToUserCart(cProduct, user);
		}
			
	}
	
	@GetMapping("/cart-info/{userId}")
	public Cart getAllUsers(@PathVariable int userId){
		Optional<User> user = userRepo.findById(userId);
		if(user.isPresent())
			return user.get().getCart();
		log.error("Invalid userId");
		return null;
	}
	
	@DeleteMapping("/clear/cart/{userId}")
	public boolean clearCart(@PathVariable int userId){
		return userService.clearCart(userId);
	}
	
	@PatchMapping("/checkoutAndPay/{userId}")
	public boolean checkout(@PathVariable int userId){
		return userService.checkoutAndPay(userId);
	}
	
	@GetMapping("/health")
	public String defaultResponse(){
		return "Api is up and running";
	}

}
