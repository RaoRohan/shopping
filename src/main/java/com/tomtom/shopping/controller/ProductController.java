package com.tomtom.shopping.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tomtom.shopping.entity.Product;
import com.tomtom.shopping.service.ProductService;

@RestController
public class ProductController {
	
	@Autowired 
	private ProductService productService;
	
	@GetMapping("/products")
	public List<Product> getProducts(){
		return productService.getProducts();
	}
	
	@PostMapping("/seller/add/product")
	public Product addProduct(@RequestBody Product product ){
		return productService.addProduct(product);
	}

}
