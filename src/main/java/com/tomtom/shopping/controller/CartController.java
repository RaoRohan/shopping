package com.tomtom.shopping.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.tomtom.shopping.dao.CartRepository;
import com.tomtom.shopping.dao.UserRepository;
import com.tomtom.shopping.entity.Cart;
import com.tomtom.shopping.entity.User;


@RestController
public class CartController {
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private CartRepository cartRepo;
	
	@GetMapping("/cart/{userId}")
	public Cart getCartByUserId(@PathVariable int userId ){
		Optional<User>  user = userRepo.findById(userId);
		if(user.isPresent()){
			Cart c = user.get().getCart();
			return c;
		}
		return null;
	}

}
