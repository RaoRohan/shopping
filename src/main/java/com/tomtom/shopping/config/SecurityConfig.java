/*package com.tomtom.shopping.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Override
	public void configure(WebSecurity web){
		
		web.ignoring().antMatchers("/");
		
	}
	
	@Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		
		auth.inMemoryAuthentication()
			.withUser("rohan").password("test").roles("USER").and()
			.withUser("rahul").password("test").roles("ADMIN");
	}
	
	
	@Override
	public void configure(HttpSecurity http) throws Exception{
		
		http.httpBasic().disable();
		http.authorizeRequests()
			.antMatchers("/seller").hasRole("ADMIN")
			.antMatchers("/user").hasRole("USER")
			.antMatchers("/").permitAll();
		
	}
	
	@Bean
	public PasswordEncoder getPasswordEncoder(){
		return NoOpPasswordEncoder.getInstance();
	}
	

}
*/