package com.tomtom.shopping.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tomtom.shopping.dao.UserRepository;
import com.tomtom.shopping.entity.Cart;
import com.tomtom.shopping.entity.User;
import com.tomtom.shopping.entity.UserPayment;

@Service
public class UserService {
	
	Logger log = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	private UserRepository userRepo;
	
	public boolean clearCart(int userId){
		
		Optional<User> optionalUser = userRepo.findById(userId);
		if(optionalUser.isPresent()){
			User user = optionalUser.get();
			Cart cart = user.getCart();
			cart.setProducts(null);
			cart.setSubTotal(0D);
			userRepo.save(user);
			log.info("cart cleared for user :- {}",userId);
			return true;
		}
		log.error("something wen wrong");
		return false;
		
	}
	
	public boolean checkoutAndPay(int userId){
		Optional<User> optionalUser = userRepo.findById(userId);
		if(optionalUser.isPresent()){
			User user = optionalUser.get();
			Cart cart = user.getCart();
			if(cart.getSubTotal() > 0){
				// make Payment
				// after successfull payment clear cart
				UserPayment currentUserWallet = user.getUserPayment();
				if(currentUserWallet.getBalance() > cart.getSubTotal()){
					currentUserWallet.setBalance(currentUserWallet.getBalance() - cart.getSubTotal());
					user.setUserPayment(currentUserWallet);
					cart.setProducts(null);
					cart.setSubTotal(0D);
					userRepo.save(user);
					log.info("order placed. Happy shopping :- {}",userId);
					return true;
				}
				
			}
		}
		log.error("something wen wrong");
		return false;
	}

}
