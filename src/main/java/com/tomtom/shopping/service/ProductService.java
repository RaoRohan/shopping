package com.tomtom.shopping.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tomtom.shopping.dao.CartRepository;
import com.tomtom.shopping.dao.ProductRespository;
import com.tomtom.shopping.entity.Cart;
import com.tomtom.shopping.entity.Product;
import com.tomtom.shopping.entity.User;
import com.tomtom.shopping.model.CartProduct;

@Service
public class ProductService{
	
	Logger log = LoggerFactory.getLogger(ProductService.class);
	
	@Autowired
	private ProductRespository productRepo;
	
	@Autowired
	private CartRepository cartRepo;
	

	public Product addProduct(Product product){
		return productRepo.save(product);
	}


	public List<Product> getProducts() {
		return productRepo.findAll();
	}


	public boolean addProductToUserCart(CartProduct cProduct, User user) {
		Cart cart = user.getCart();
		Optional<Product> existingProduct = productRepo.findById(cProduct.getProductId());
		if(existingProduct.isPresent()){
			cart.addProductToCart(existingProduct.get());
			cart.setSubTotal(cart.getUpdatedSubTotal());
			cartRepo.save(cart);
			log.info("Product with ID :- {} added to cart",cProduct.getProductId());
		} else {
			log.error("Invalid Product : {}", cProduct.getProductId());
			return false;
		}
			
		return true;
		
		
	}


	
}