package com.tomtom.shopping.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tomtom.shopping.entity.Product;

public interface ProductRespository extends JpaRepository<Product, Long> {

}
