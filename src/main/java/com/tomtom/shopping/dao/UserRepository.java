package com.tomtom.shopping.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tomtom.shopping.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

}
