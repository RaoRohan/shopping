package com.tomtom.shopping.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tomtom.shopping.entity.Cart;

@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {
	
	/*@Modifying
	@Query("update CART c set c.products = ?1 where c.cartId = ?2")
	public void updateCart(List<Product> products, Long cartId);*/

}
